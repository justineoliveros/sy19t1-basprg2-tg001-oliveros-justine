// ConsoleApplication17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "string"
#include "time.h"
#include "conio.h"

using namespace std;

struct Item {

	string name;
	int gold;

};

int enterDungeon(int &userGold, int dungeonFee, Item itemArray[], int &multiplier, bool &keepPlaying)
{
	int itemRandomizer = rand() % 5;
	char choice;

	cout << "Do you want to enter the dungeon? Y/N: ";	// continuously will ask if you want to go to the dungeon
	cin >> choice;

	switch (choice)
	{
	case 'Y':	// If Player chooses Y then...
	case 'y':
		userGold -= dungeonFee;	// Deducts gold and checks the following requirements below. v

		if (itemArray[itemRandomizer].name == "Mithril Ore")
		{
			userGold = (itemArray[itemRandomizer].gold * multiplier) + userGold;
			cout << "You got a Mithril Ore worth 100! Current Gold: " << userGold << " Current Multiplier: x" << multiplier << endl;
			multiplier++;
			return userGold;
		}
		else if (itemArray[itemRandomizer].name == "Sharp Talon")
		{
			userGold = (itemArray[itemRandomizer].gold * multiplier) + userGold;
			cout << "You got a Sharp Talon worth 50! Current Gold: " << userGold << " Current Multiplier: x" << multiplier << endl;
			multiplier++;
			return userGold;
		}
		else if (itemArray[itemRandomizer].name == "Thick Leather")
		{
			userGold = (itemArray[itemRandomizer].gold * multiplier) + userGold;
			cout << "You got a Thick Leather worth 25! Current Gold: " << userGold << " Current Multiplier: x" << multiplier << endl;
			multiplier++;
			return userGold;
		}
		else if (itemArray[itemRandomizer].name == "Jellopy")
		{
			userGold = (itemArray[itemRandomizer].gold * multiplier) + userGold;
			cout << "You got a Jellopy 5! Current Gold: " << userGold << " Current Multiplier: x" << multiplier << endl;
			multiplier++;
			return userGold;
		}
		else if (itemArray[itemRandomizer].name == "Cursed Stone")
		{
			userGold = userGold - userGold;
			cout << "Uh Oh, You got a Cursed Stone. Current Gold: " << userGold << endl;
			multiplier = 1;
			return userGold;
		}

	case 'n':
	case 'N':	// If N Multiplier ends up going back to normal, and loops back to this function
		multiplier = 1;
		cout << "Okay coward for leaving your Multiplier has been reset to 1! Your Current Gold is " << userGold << endl;
		_getch();
		enterDungeon(userGold, 25, itemArray, multiplier, keepPlaying);
	}

}

void gameCondition (bool &keepPlaying)
{
	while (keepPlaying)	// Referenced the bool here so we can stop it after
	{
		cout << "Game Over!";
		keepPlaying = false;
	}

}

int main()
{
	srand(time(NULL));
	int userGold = 50;
	bool keepPlaying = true;
	int multiplier = 1;
	Item itemArray[5];
	itemArray[0].name = "Mithril Ore";
	itemArray[0].gold = 100;
	itemArray[1].name = "Sharp Talon";
	itemArray[1].gold = 50;
	itemArray[2].name = "Thick Leather";
	itemArray[2].gold = 25;
	itemArray[3].name = "Jellopy";
	itemArray[3].gold = 5;
	itemArray[4].name = "Cursed Stone";
	itemArray[4].gold = 0;

	while (keepPlaying)	// main sequence
	{
		enterDungeon(userGold, 25, itemArray, multiplier, keepPlaying);	
		if (userGold <= 0 || userGold < 25 || userGold >= 500)
		{
			gameCondition(keepPlaying);
		}
		_getch();
	}

	return 0;
}
