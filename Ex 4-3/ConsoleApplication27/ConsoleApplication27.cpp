// ConsoleApplication10.cpp : Defines the entry point for the console application.
//

#include "pch.h"
#include "string"
#include "conio.h"
#include "iostream"
#include "time.h"
#include "Wizard.h"

using namespace std;

int main()
{
	srand(time(NULL));
	Wizard *wizardMc = new Wizard;
	Wizard *wizardNotMc = new Wizard;
	Spell *fireBall = new Spell;

	wizardMc->name = "Colinsuke";
	wizardMc->hp = 100;
	wizardMc->mana = 50;

	wizardNotMc->name = "Seanaruto";
	wizardNotMc->hp = 100;
	wizardNotMc->mana = 50;

	fireBall->name = "Fireball Jutsu";
	fireBall->manaCost = rand() % 5 + 1;

	while (true)
	{
		cout << "Remaining Stats:\n";
		cout << "=============================\n";
		wizardMc->viewStats(wizardMc);
		wizardNotMc->viewStats(wizardNotMc);
		cout << endl;

		fireBall->damage = rand() % 20 + 1;
		fireBall->manaCost = rand() % 5 + 1;

		if (wizardMc->hp > 0 && wizardMc->mana > 0)
		{
			wizardMc->attackWizard(wizardNotMc, fireBall);
			_getch();
		}
		else if (wizardMc->hp <= 0 || wizardMc->mana <= 0)
		{
			cout << wizardNotMc->name << " wins!\n";
			cout << wizardNotMc->name << ": Mahina ka pa, palakas ka muna...\n";
			cout << wizardMc->name << ": Ano? Masaya ka na? Sinaktan mo ko?";
			break;
		}

		fireBall->damage = rand() % 20 + 1;
		fireBall->manaCost = rand() % 5 + 1;

		if (wizardNotMc->hp > 0 && wizardNotMc->mana > 0)
		{
			wizardNotMc->attackWizard(wizardMc, fireBall);
			_getch();
		}
		else if (wizardNotMc->hp <= 0 || wizardNotMc->mana <= 0)
		{
			cout << wizardMc->name << " wins!\n";
			cout << wizardMc->name << ": Mahina ka pa tol...\n";
			cout << wizardNotMc->name << ": Stop being insecure!\n";
			break;
		}
	}

	_getch();
	return 0;
}
