#include "pch.h"
#include "Wizard.h"
#include "Spell.h"


void Wizard::attackWizard(Wizard *target, Spell *fireBall)
{
	cout << this->name << " uses " << fireBall->manaCost << " chakra and attacks " << target->name << " using " << fireBall->name << " dealing " << fireBall->damage << " damage!" << endl << endl;
	this->mana -= fireBall->manaCost;
	target->hp -= fireBall->damage;
}

void Wizard::viewStats(Wizard *target)
{
	cout << this->name << " HP" << " = " << this->hp << endl;
	cout << this->name << " Mana/Chakra" << " = " << this->mana << endl;
}