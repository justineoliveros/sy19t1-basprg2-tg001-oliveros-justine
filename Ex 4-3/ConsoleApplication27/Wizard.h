#pragma once
#include <iostream>
#include "string"
#include "Spell.h"
#include "Wizard.h"

using namespace std;

class Wizard
{
public:
	string name;
	int mana;
	int hp;

	void attackWizard(Wizard *target, Spell *fireBall);
	void viewStats(Wizard *target);

};