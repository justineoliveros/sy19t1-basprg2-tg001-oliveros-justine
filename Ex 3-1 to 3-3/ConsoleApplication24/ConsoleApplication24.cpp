// ConsoleApplication24.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "string"
#include <conio.h>
#include <time.h>

using namespace std;

void arrayRandomizer(int *number)
{
	int i;

	for (i = 0; i < 10; i++) //  randomizer for arrays
	{
		number[i] = rand() % 100 + 1;
	}

	for (i = 0; i < 10; i++) // inputs it
	{
		cout << number[i] << endl;
	}

}

void dynamicArrayRandomizer(int *numbers) 
{
	int i;

	for (i = 0; i < 10; i++) //  randomizer for arrays
	{
		numbers[i] = rand() % 100 + 1;
	}

	for (i = 0; i < 10; i++) // inputs it
	{
		cout << numbers[i] << endl;
	}

	_getch();

	delete numbers;	// deletes the randomized numbers in the array

}

int main()
{
	srand(time(NULL));
	int number[10];
	int *numbers;
	numbers = new int[10];	// creates the array as a "new" int 

	cout << "Ex 3-1\n";
	arrayRandomizer(number); // passes array "number" to function

	_getch();
	
	cout << "Ex 3-2 & 3-3\n";
	dynamicArrayRandomizer(numbers); // passes it to the function

	return 0;
}