#pragma once
#include "string"
#include "iostream"
#include "Weapon.h"
#include "Accessory.h"
#include "Arm.h"

using namespace std;

class Cloud
{
public:
	Cloud();
	Cloud(int mLvl, int mHp, int mMp, int mExp);
	void viewStats();
	void attackWeap();
	void attackMag();
	void specialAttack();
	void usePotion();
	void movePlayer();
	void interactNpc();
	
private:
	int mNextLevel;
	int mLimitLevel;
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;
	int mAttack;
	float mAttackPerc;
	int mDefense;
	float mDefensePerc;
	int mMagicAtk;
	int mMagicDef;
	float mMagicDefPerc;
};

