#pragma once
#include "string"
#include "iostream"
#include "Weapon.h"
#include "Accessory.h"
#include "Cloud.h"

using namespace std;

class Arm
{
public:
	Arm();
	void addArmorStats();

private:
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;
	
};

