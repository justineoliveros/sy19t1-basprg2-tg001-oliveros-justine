#pragma once
#include "string"
#include "iostream"
#include "Weapon.h"
#include "Arm.h"
#include "Cloud.h"

using namespace std;

class Accessory
{
public:
	Accessory();
	void addAccStats();
	
private:
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;
};

