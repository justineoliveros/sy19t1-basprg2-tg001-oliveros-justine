#pragma once
#include "string"
#include "iostream"
#include "Accessory.h"
#include "Cloud.h"
#include "Arm.h"

using namespace std;

class Weapon
{
public:
	Weapon();
	addWeaponStats();
private:
	int mStrength;
	int mDexterity;
	int mVitality;
	int mMagic;
	int mSpirit;
	int mLuck;
};

