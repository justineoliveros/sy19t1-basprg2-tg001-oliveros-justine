#pragma once
#include "Shape.h"

class Square : public Shape
{
public:
	Square();
	~Square();

	float getLength();
	void setLength(int value);
	float getArea() override;
	string getName();
	float getNumSides();

private:
	float mLength;
	string mName = "Square";
	float mNumSides = 4;
};

