#include "pch.h"
#include "Circle.h"


Circle::Circle()
{
}

void Circle::setLength(int value)
{
	mLength = value;
}

string Circle::getName()
{
	return mName;
}

float Circle::getNumSides()
{
	return mNumSides;
}

float Circle::getLength()
{
	return mLength;
}

float Circle::getArea()
{
	return 3.14 * mLength * mLength;
}

Circle::~Circle()
{
}
