// ConsoleApplication30.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "vector"
#include "Square.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Shape.h"

using namespace std;

int main()
{
	vector<Shape*> shapes;
	int valueSquare;
	float radiusCircle;
	int valueRectangle;
	int valueWidth;
    
	Square* square = new Square;
	Rectangle* rectangle = new Rectangle;
	Circle* circle = new Circle;
	shapes.push_back(square);
	shapes.push_back(circle);
	shapes.push_back(rectangle);

	cout << "Input length of the Square: ";
	cin >> valueSquare;
	square->setName("Square");
	square->setNumSides(4);
	square->setLength(valueSquare);

	system("pause");
	system("cls");

	cout << "Input radius of the Circle: ";
	cin >> radiusCircle;
	circle->setName("Circle");
	circle->setNumSides(1);
	circle->setLength(radiusCircle);

	system("pause");
	system("cls");

	cout << "Input length of the Rectangle: ";
	cin >> valueRectangle;
	cout << "Input width of the Rectangle: ";
	cin >> valueWidth;
	rectangle->setName("Rectangle");
	rectangle->setNumSides(4);
	rectangle->setLength(valueRectangle);
	rectangle->setWidth(valueWidth);

	system("pause");
	system("cls");

	for (int i = 0; i < shapes.size(); i++)
	{
		Shape* shape = shapes[i];
		cout << "Name: " << shape->getName() << endl;
		cout << "Sides: " << shape->getNumSides() << endl;
		cout << "Area: " << shape->getArea() << endl;
		cout << endl;
	}

}

