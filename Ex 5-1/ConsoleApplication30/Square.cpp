#include "pch.h"
#include "Square.h"


Square::Square()
{
}

void Square::setLength(int value)
{
	mLength = value;
}

float Square::getLength()
{
	return mLength;
}

float Square::getNumSides()
{
	return mNumSides;
}

string Square::getName()
{
	return mName;
}

float Square::getArea()
{
	return mLength * mLength;
}

Square::~Square()
{
}
