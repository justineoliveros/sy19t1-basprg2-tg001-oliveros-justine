#include "pch.h"
#include "Rectangle.h"


Rectangle::Rectangle()
{
}

void Rectangle::setLength(int value)
{
	mLength = value;
}

void Rectangle::setWidth(int width)
{
	mWidth = width;
}

float Rectangle::getNumSides()
{
	return mNumSides;
}

string Rectangle::getName()
{
	return mName;
}

float Rectangle::getLength()
{
	return mLength;
}

float Rectangle::getArea()
{
	return mLength * mWidth;
}

Rectangle::~Rectangle()
{
}
