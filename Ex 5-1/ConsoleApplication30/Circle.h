#pragma once
#include "Shape.h"

class Circle : public Shape
{
public:
	Circle();
	~Circle();

	float getLength();
	string getName();
	float getNumSides();
	void setLength(int value);
	float getArea() override;

private:
	float mLength;
	string mName = "Circle";
	float mNumSides = 1;
};

