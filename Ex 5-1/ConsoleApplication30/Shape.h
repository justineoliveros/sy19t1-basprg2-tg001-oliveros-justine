#pragma once
#include "string"

using namespace std;

class Shape
{
public:
	Shape();
	~Shape();

	string getName();
	float getNumSides();

	void setName(string name);
	void setNumSides(int numSides);

	virtual float getArea();

private:
	string mName;
	int mNumSides;
};

