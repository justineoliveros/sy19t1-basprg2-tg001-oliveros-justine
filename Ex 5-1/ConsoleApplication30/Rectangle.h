#pragma once
#include "Shape.h"

class Rectangle : public Shape
{
public:
	Rectangle();

	float getLength();
	void setLength(int value);
	void setWidth(int width);
	string getName();
	float getNumSides();
	float getArea() override;

	~Rectangle();

private:
	float mLength;
	float mWidth;
	float mNumSides = 4;
	string mName = "Rectangle";
};

