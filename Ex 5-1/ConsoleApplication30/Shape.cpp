#include "pch.h"
#include "Shape.h"


Shape::Shape()
{
}

string Shape::getName()
{
	return mName;
}

float Shape::getNumSides()
{
	return mNumSides;
}

void Shape::setName(string name)
{
	mName = name;
}

void Shape::setNumSides(int numSides)
{
	mNumSides = numSides;
}

float Shape::getArea()
{
	return 0;
}

Shape::~Shape()
{
}
