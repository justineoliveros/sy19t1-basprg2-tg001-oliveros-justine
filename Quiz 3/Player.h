#pragma once
#include"string"
#include "Assassin.h"
#include "Warrior.h"
#include "Mage.h"

using namespace std;

class Player : public Warrior, public Assassin, public Mage
{
public:
	void setPlayerStats(int choiceClass, string name);
	void viewPlayerStats();
	string getPlayerName();
	int getPlayerHp();
	int getPlayerPow();
	int getPlayerVit();
	int getPlayerAgi();
	int getPlayerDex();
	~Player();

protected:
	string mName;
	int mHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mAttackDmg;
	int mHitRate;
};

