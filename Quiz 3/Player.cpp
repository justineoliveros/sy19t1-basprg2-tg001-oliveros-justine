#include "pch.h"
#include "Player.h"
#include "string"
#include "iostream"

using namespace std;

void Player::setPlayerStats(int choiceClass, string name)
{
	if (choiceClass == 1)
	{
		mName = name;
		mHp = mWarriorHp;
		mPow = mWarriorPow;
		mVit = mWarriorVit;
		mAgi = mWarriorAgi;
		mDex = mWarriorDex;
	}
	else if (choiceClass == 2)
	{
		mName = name;
		mHp = mAssassinHp;
		mPow = mAssassinPow;
		mVit = mAssassinVit;
		mAgi = mAssassinAgi;
		mDex = mAssassinDex;
	}
	else if (choiceClass == 3)
	{
		mName = name;
		mHp = mMageHp;
		mPow = mMagePow;
		mVit = mMageVit;
		mAgi = mMageAgi;
		mDex = mMageDex;
	}
}

void Player::viewPlayerStats()
{
	cout << "Name: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "POW: " << mPow << endl;
	cout << "VIT: " << mVit << endl;
	cout << "AGI: " << mAgi << endl;
	cout << "DEX: " << mDex << endl;
}

string Player::getPlayerName()
{
	return mName;
}

int Player::getPlayerHp()
{
	return mHp;
}

int Player::getPlayerPow()
{
	return mPow;
}

int Player::getPlayerVit()
{
	return mVit;
}

int Player::getPlayerAgi()
{
	return mAgi;
}

int Player::getPlayerDex()
{
	return mDex;
}

Player::~Player()
{
}
