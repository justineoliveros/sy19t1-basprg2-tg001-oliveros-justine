#include "pch.h"
#include "Enemy.h"
#include "iostream"

using namespace std;

void Enemy::setEnemyStats(int choiceClass)
{
	if (choiceClass == 1)
	{
		mAiName = "Enemy Warrior";
		mAiHp = mWarriorHp;
		mAiPow = mWarriorPow;
		mAiVit = mWarriorVit;
		mAiAgi = mWarriorAgi;
		mAiDex = mWarriorDex;
	}
	else if (choiceClass == 2)
	{
		mAiName = "Enemy Assassin";
		mAiHp = mAssassinHp;
		mAiPow = mAssassinPow;
		mAiVit = mAssassinVit;
		mAiAgi = mAssassinAgi;
		mAiDex = mAssassinDex;
	}
	else if (choiceClass == 3)
	{
		mAiName = "Enemy Mage";
		mAiHp = mMageHp;
		mAiPow = mMagePow;
		mAiVit = mMageVit;
		mAiAgi = mMageAgi;
		mAiDex = mMageDex;
	}
}

void Enemy::viewEnemyStats()
{
	cout << "Name: " << mAiName << endl;
	cout << "HP: " << mAiHp << endl;
	cout << "POW: " << mAiPow << endl;
	cout << "VIT: " << mAiVit << endl;
	cout << "AGI: " << mAiAgi << endl;
	cout << "DEX: " << mAiDex << endl;
}

string Enemy::getEnemyName()
{
	return mAiName;
}

int Enemy::getEnemyHp()
{
	return mAiHp;
}

int Enemy::getEnemyPow()
{
	return mAiPow;
}

int Enemy::getEnemyVit()
{
	return mAiVit;
}

int Enemy::getEnemyAgi()
{
	return mAiAgi;
}

int Enemy::getEnemyDex()
{
	return mAiDex;
}

Enemy::~Enemy()
{
}
