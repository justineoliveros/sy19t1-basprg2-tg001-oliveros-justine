#pragma once

using namespace std; 

class Mage
{
public:
	Mage();
	~Mage();

protected:
	int mMageHp = 15;
	int mMagePow = 10;
	int mMageVit = 3;
	int mMageAgi = 3;
	int mMageDex = 6;
};

