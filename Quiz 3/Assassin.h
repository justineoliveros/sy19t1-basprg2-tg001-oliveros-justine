#pragma once

using namespace std;

class Assassin
{
public:
	Assassin();
	~Assassin();

protected:
	int mAssassinHp = 20;
	int mAssassinPow = 7;
	int mAssassinVit = 3;
	int mAssassinAgi = 10;
	int mAssassinDex = 8;
};

