#pragma once
#include "string"
#include "Assassin.h"
#include "Warrior.h"
#include "Mage.h"

using namespace std;

class Enemy : public Warrior, public Assassin, public Mage
{
public:
	void setEnemyStats(int choiceClass);
	void viewEnemyStats();
	string getEnemyName();
	int getEnemyHp();
	int getEnemyPow();
	int getEnemyVit();
	int getEnemyAgi();
	int getEnemyDex();
	~Enemy();

protected:
	string mAiName;
	int mAiHp;
	int mAiPow;
	int mAiVit;
	int mAiAgi;
	int mAiDex;
};

