// ConsoleApplication27.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "Player.h"
#include "Enemy.h"
#include "Assassin.h"
#include "Warrior.h"
#include "Mage.h"
#include <iostream>
#include "string"
#include "time.h"

using namespace std;

int main()
{
	srand(time(NULL));
	int classChoice;
	string name;
	
	cout << "Input Name: ";
	cin >> name;
	cout << endl;
	cout << "Choose Class/Job:\n";
	cout << "[1] Warrior\n";
	cout << "[2] Assassin\n";
	cout << "[3] Mage\n";
	cin >> classChoice;

	system("pause");
	system("cls");

	Player* player1 = new Player;
	Enemy* enemy1 = new Enemy;

	while (true)
	{
		player1->setPlayerStats(classChoice, name);
		player1->viewPlayerStats();

		cout << endl;
		cout << "vs\n\n";

		enemy1->setEnemyStats(rand() % 3 + 1);
		enemy1->viewEnemyStats();

		system("pause");
		system("cls");
	}
}
