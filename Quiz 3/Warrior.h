#pragma once

using namespace std;

class Warrior
{
public:
	Warrior();
	~Warrior();

protected:
	int mWarriorHp = 25;
	int mWarriorPow = 3;
	int mWarriorVit = 10;
	int mWarriorAgi = 3;
	int mWarriorDex = 3;
};

