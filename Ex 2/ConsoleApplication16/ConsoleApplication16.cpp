// ConsoleApplication16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "conio.h"
#include "string"
#include "iostream"
#include "time.h"

using namespace std;

int askBet(int &playerGold, int &bet) // Ex 2-1 Bet
{
	return playerGold -= bet;	// returns playerGold - bet
}

/*void askBet(int &playerGold, int &bet) 
{
	playerGold -= bet;	// Pretty much the same but using void
}*/

void diceRoll(int &playerDiceOne, int &playerDiceTwo, int &playerRoll, int &aiDiceOne, int &aiDiceTwo, int &aiRoll)	// Ex 2-2 Dice Roll 
{
	playerDiceOne = rand() % 6 + 1;	// Player Dices
	playerDiceTwo = rand() % 6 + 1;	
	aiDiceOne = rand() % 6 + 1;	// AI Dices
	aiDiceTwo = rand() % 6 + 1;

	playerRoll = playerDiceOne + playerDiceTwo;	// Add them up to get the number of rolls
	aiRoll = aiDiceOne + aiDiceTwo;

}

int payOut(int &playerGold, int bet, int playerRoll, int aiRoll)	// Ex 2-3 Payout
{
	if (playerRoll > aiRoll && aiRoll != 2)
	{
		playerGold = playerGold + bet + bet;	// Added another +bet so I can give back the subtracted bet from Ex 2-1.
		cout << "Player rolled a, " << playerRoll << "! AI rolled a, " << aiRoll << "! Player WINS!\n";
	}
	else if (aiRoll > playerRoll && playerRoll != 2)
	{
		playerGold = playerGold - bet;
		cout << "Player rolled a, " << playerRoll << "! AI rolled a, " << aiRoll << "! AI WINS!\n";
	}
	else if (playerRoll == 2)
	{
		playerGold = playerGold + bet + bet + bet;	// Same principle but added another +bet bonus for snake eyes.
		cout << "1-1 sssSNAKE EYESsss! Player wins! with a total of " << playerRoll << " to AI's " << aiRoll << "!\n";
	}
	else if (aiRoll == 2)
	{
		playerGold = playerGold - bet - bet;	// I didn't use another -bet since Ex 2-1 already got that covered.
		cout << "1-1 sssSNAKEEEE EYESSSSssss! AI wins! with a total of " << aiRoll << " to Player's " << playerRoll << "!\n";
	}
	else if (aiRoll == playerRoll)
	{
		playerGold = playerGold + bet;	// Added +bet to gain back lost bet from Ex 2-1.
		cout << "oof same rolls! RESTART!" << endl;
	}

	return playerGold;
}

void playRound(int &playerGold, bool &playing)
{
	while (playing)	// Referenced the bool from main to here so I can stop it here.
	{
		cout << "Game Over!";
		playing = false;	// Loop ender.
	}
	
}

int main()
{
	srand(time(NULL));
	int bet;
	int playerGold = 1000;
	bool playing = true;
	int playerDiceOne = rand() % 6 + 1;
	int playerDiceTwo = rand() % 6 + 1;
	int aiDiceOne = rand() % 6 + 1;
	int aiDiceTwo = rand() % 6 + 1;
	int aiRoll = aiDiceOne + aiDiceTwo;
	int playerRoll = playerDiceOne +playerDiceTwo;

	while (playing)	// Main Flow
	{
		cout << "You have " << playerGold << ", How much do you want to bet? ";
		cin >> bet;
		if (bet <= playerGold)	
		{
			askBet(playerGold, bet);
			diceRoll(playerDiceOne, playerDiceTwo, playerRoll, aiDiceOne, aiDiceTwo, aiRoll);
			payOut(playerGold, bet, playerRoll, aiRoll);

			if (playerGold <= 0)
			{
				playRound(playerGold, playing);
			}
		}
		else if (bet <= 0 || bet > playerGold) // Restarts loop if bet is less than/equal 0 or if bet is higher than playerGold.
		{
			cout << "Invalid, son.\n";
			playing = true;	
		}
	}

	_getch();
	return 0;
}