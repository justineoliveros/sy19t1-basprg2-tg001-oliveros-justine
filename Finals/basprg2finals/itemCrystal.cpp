#include "pch.h"
#include "itemCrystal.h"

itemCrystal::itemCrystal(string name, int addCrystal) : Item(name)
{
	mCrystal = addCrystal;
}

itemCrystal::~itemCrystal()
{
}

void itemCrystal::gachaRoll(Unit* target)
{
	Item::gachaRoll(target);

	target->addCrystal(mCrystal);
	cout << "You got a Crystal! You've gained " << mCrystal << " crystals!" << endl;
}
