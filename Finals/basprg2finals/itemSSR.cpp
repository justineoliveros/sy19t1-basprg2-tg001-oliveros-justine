#include "pch.h"
#include "itemSSR.h"

itemSSR::itemSSR(string name, int point) : Item(name)
{
	mPoint = point;
}

itemSSR::~itemSSR()
{
}

void itemSSR::gachaRoll(Unit * target)
{
	Item::gachaRoll(target);

	target->ssrPoint(mPoint);
	cout << "You pulled an SSR! You got " << mPoint << " rarity points!" << endl;
}
