#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

class Item;

using namespace std;

class Unit
{
public:
	Unit(int hp, int crystal, int raritypoints);
	~Unit();

	int getHP();
	int getCrystal();
	int getRarityPoints();
	int getRolls();

	vector<Item*>& getItem();

	void numberOfRolls();
	void payCrystal();
	void addItem(Item * item);
	void addCrystal(int wonCrystal);
	void heal(int heal);
	void bomb(int damage);
	void rPoint(int rPoints);
	void srPoint(int rPoints);
	void ssrPoint(int rPoints);

	void pullRandom();

	bool isAlive();
	bool canPull();
	bool playerWon();

	void rCounter();
	void srCounter();
	void ssrCounter();
	void potionCounter();
	void bombCounter();
	void crystalCounter();

	void displayStats();
	void displayScore();

private:
	int mHp;
	int mCrystal;
	int mRarityPoints;
	int mRolls;
	int mPulledSSR;
	int mPulledSR;
	int mPulledR;
	int mPulledPotion;
	int mPulledBomb;
	int mPulledCrystals;
	vector <Item*> mItem;
};

