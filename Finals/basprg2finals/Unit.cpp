#include "pch.h"
#include "Unit.h"

Unit::Unit(int hp, int crystal, int raritypoints)
{
	mHp = hp;
	mCrystal = crystal;
	mRarityPoints = raritypoints;
}

Unit::~Unit()
{
	for (int i = 0; i < mItem.size(); i++)
		delete mItem[i];  

	mItem.clear();
}

int Unit::getHP()
{
	return mHp;
}

int Unit::getCrystal()
{
	return mCrystal;
}

int Unit::getRarityPoints()
{
	return mRarityPoints;
}

int Unit::getRolls()
{
	return mRolls;
}

vector<Item*>& Unit::getItem()
{
	return mItem;
}    

void Unit::numberOfRolls()
{
	mRolls += 1;
}

void Unit::payCrystal()
{
	mCrystal -= 5;
}

void Unit::addItem(Item * item)
{
	item->setActor(this);

	mItem.push_back(item);
}

void Unit::addCrystal(int wonCrystal)
{
	mCrystal += 15;
}

void Unit::heal(int heal)
{
	mHp += heal;
}

void Unit::bomb(int damage)
{
	mHp -= damage;

	if (mHp < 0)
		mHp = 0;
}

void Unit::rPoint(int points)
{
	mRarityPoints += 1;
}

void Unit::srPoint(int points)
{
	mRarityPoints += 10;
}

void Unit::ssrPoint(int points)
{
	mRarityPoints += 50;
}

void Unit::pullRandom()
{
	int itemRandomizer = rand()% 100 + 1;
	if (itemRandomizer == 1)
	{
		Item* pull = this->getItem()[0];
		pull->gachaRoll(this);

		this->ssrCounter();
		this->numberOfRolls();
	}
	else if (itemRandomizer < 10)
	{
		Item* pull = this->getItem()[1];
		pull->gachaRoll(this);

		this->srCounter();
		this->numberOfRolls();
	}
	else if (itemRandomizer <= 40)
	{
		Item* pull = this->getItem()[2];
		pull->gachaRoll(this);

		this->rCounter();
		this->numberOfRolls();
	}
	else if (itemRandomizer <= 55)
	{
		Item* pull = this->getItem()[3];
		pull->gachaRoll(this);

		this->potionCounter();
		this->numberOfRolls();
	}
	else if (itemRandomizer <= 75)
	{
		Item* pull = this->getItem()[4];
		pull->gachaRoll(this);

		this->bombCounter();
		this->numberOfRolls();
	}
	else if (itemRandomizer <= 100)
	{
		Item* pull = this->getItem()[5];
		pull->gachaRoll(this);

		this->crystalCounter();
		this->numberOfRolls();
	}
}

bool Unit::isAlive()
{
	if (mHp > 0)
		return true;
	else if (mHp <= 0)
		return false;
}

bool Unit::canPull()
{
	if (mCrystal >= 5)
		return true;
	else if (mCrystal < 5)
		return false;
}

bool Unit::playerWon()
{
	if (mRarityPoints >= 100)
		return true;
	else if (mRarityPoints < 100)
		return false;
}

void Unit::rCounter()
{
	mPulledR++;
}

void Unit::srCounter()
{
	mPulledSR++;
}

void Unit::ssrCounter()
{
	mPulledSSR++;
}

void Unit::potionCounter()
{
	mPulledPotion++;
}

void Unit::bombCounter()
{
	mPulledBomb++;
}

void Unit::crystalCounter()
{
	mPulledCrystals++;
}

void Unit::displayStats()
{
	cout << "HP: " << mHp << endl;
	cout << "Crystals: " << mCrystal << endl;
	cout << "Rarity Points: " << mRarityPoints << endl;
	cout << "Rolls: " << mRolls << endl;
}

void Unit::displayScore()
{
	cout << "Items Pulled:\n";
	cout << "R: x" << mPulledR << endl;
	cout << "SR: x" << mPulledSR << endl;
	cout << "SSR: x" << mPulledSSR << endl;
	cout << "Potion: x" << mPulledPotion << endl;
	cout << "Bomb: x" << mPulledBomb << endl;
	cout << "Crystal: x" << mPulledCrystals << endl;
	cout << "Rarity Points: " << mRarityPoints << endl;
}
