#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class itemSR : public Item
{
public:
	itemSR(string name, int point);
	~itemSR();

	void gachaRoll(Unit* target);

private:
	int mPoint;
};
