#include "pch.h"
#include "itemR.h"

itemR::itemR(string name, int point) : Item(name)
{
	mPoint = point;
}

itemR::~itemR()
{
}

void itemR::gachaRoll(Unit * target)
{
	Item::gachaRoll(target);

	target->rPoint(mPoint);
	cout << "You pulled an R! You got " << mPoint << " rarity point!" << endl;
}
