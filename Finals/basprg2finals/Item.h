#pragma once
#include <iostream>
#include <vector>
#include "Unit.h"
using namespace std;

class Unit;

class Item
{
public:
	Item(string name);
	~Item();

	string getName();
	Unit* getActor();
	void setActor(Unit* actor);

	virtual void gachaRoll(Unit* target);

private:
	string mName;
	Unit* mActor;
};

