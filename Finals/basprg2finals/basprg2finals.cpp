#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"
#include "itemR.h"
#include "itemSR.h"
#include "itemSSR.h"
#include "itemPotion.h"
#include "itemCrystal.h"
#include "itemBomb.h"
using namespace std;

Unit * createUnit()
{
	Unit* unit = new Unit(100, 100, 0);

	Item* superSuperRare = new itemSSR("SSR", 50);
	unit->addItem(superSuperRare);

	Item* superRare = new itemSR("SR", 10);
	unit->addItem(superRare);

	Item* rare = new itemR("R", 1);
	unit->addItem(rare);

	Item* potion = new itemPotion("Potion", 30);
	unit->addItem(potion);

	Item* bomb = new itemBomb("Bomb", 25);
	unit->addItem(bomb);

	Item* crystal = new itemCrystal("Crystal", 15);
	unit->addItem(crystal);

	return unit;
}

void gameEnd(Unit* player)
{
	if (player->getRarityPoints() >= 100)
		cout << "Congratulations you won the game!" << endl;
	else
		cout << "Game Over!" << endl;

	player->displayStats();
	cout << endl;
	player->displayScore();

	delete player;
}

void gameRule(bool &keepPlaying, Unit* player)
{
	if (player->isAlive() == false)
		keepPlaying = false;
	else if (player->canPull() == false)
		keepPlaying = false;
	if (player->playerWon() == true)
		keepPlaying = false;
}
int main()
{
	srand(time(NULL));
	bool keepPlaying = true;
	Unit* player = createUnit();

	while (keepPlaying)
	{	
		player->displayStats();
		cout << endl;

		player->payCrystal();

		player->pullRandom();

		gameRule(keepPlaying, player);
		system("pause");
		system("cls");
	}

	gameEnd(player);
	system("pause");
	return 0;
}