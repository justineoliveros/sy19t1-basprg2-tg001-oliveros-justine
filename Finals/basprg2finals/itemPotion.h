#pragma once
#include <string>
#include <iostream>
#include "Item.h"
using namespace std;

class itemPotion : public Item
{
public:
	itemPotion(string name, int addHeal);
	~itemPotion();

	void gachaRoll(Unit* target);

private:
	int mHeal;
};
