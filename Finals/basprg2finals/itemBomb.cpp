#include "pch.h"
#include "itemBomb.h"

itemBomb::itemBomb(string name, int damage) : Item(name)
{
	mDamage = damage;
}

itemBomb::~itemBomb()
{
}

void itemBomb::gachaRoll(Unit * target)
{
	Item::gachaRoll(target);

	target->bomb(mDamage);
	cout << "You got bombed! You've been hit with " << mDamage << " damage!" << endl;
}
