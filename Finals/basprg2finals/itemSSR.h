#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class itemSSR : public Item
{
public:
	itemSSR(string name, int point);
	~itemSSR();

	void gachaRoll(Unit* target);

private:
	int mPoint;
};
