#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class itemR : public Item
{
public:
	itemR(string name, int point);
	~itemR();

	void gachaRoll(Unit* target);

private:
	int mPoint;
};

