#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class itemBomb : public Item
{
public:
	itemBomb(string name, int damage);
	~itemBomb();

	void gachaRoll(Unit* target);

private:
	int mDamage;
};