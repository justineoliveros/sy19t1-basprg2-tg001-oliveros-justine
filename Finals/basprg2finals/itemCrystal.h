#pragma once
#include "Item.h"
#include <string>
#include <iostream>
using namespace std;

class itemCrystal : public Item
{
public:
	itemCrystal(string name, int addCrystal);
	~itemCrystal();

	void gachaRoll(Unit* target);

private:
	int mCrystal;
};

