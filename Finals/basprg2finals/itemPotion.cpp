#include "pch.h"
#include "itemPotion.h"

itemPotion::itemPotion(string name, int addHeal) : Item(name)
{
	mHeal = addHeal;
}

itemPotion::~itemPotion()
{
}

void itemPotion::gachaRoll(Unit * target)
{
	Item::gachaRoll(target);

	target->heal(mHeal);
	cout << "You got a Potion! You've been healed for " << mHeal << " HP!" << endl;
}