#include "pch.h"
#include "itemSR.h"

itemSR::itemSR(string name, int point) : Item(name)
{
	mPoint = point;
}

itemSR::~itemSR()
{
}

void itemSR::gachaRoll(Unit * target)
{
	Item::gachaRoll(target);

	target->srPoint(mPoint);
	cout << "You pulled an SR! You get " << mPoint << " rarity points!" << endl;
}
