// ConsoleApplication18.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "time.h"
#include "conio.h"
#ifndef NODE_H
#define NODE_H

#include <string>

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};

#endif //NODE_H

using namespace std;


Node* sequence(Node *soldiers, Node* head)
{
	Node* temp = new Node;
	temp->name = soldiers->name; // Soldier name will be passed here.
	temp->next = NULL;	// Next will be NULL

	if (head != NULL)
	{
		temp->next = head;
	}

	head = temp;

	return head;
}

void printSoldiers(Node* head)
{
	cout << "Soldiers are... ";
	while (head != NULL)
	{
		cout << endl << head->name;
		head = head->next;

	}

	cout << endl;
}

void cloakPassing(Node* head, int numberOfSoldiers, Node* soldiers, bool& round)
{
	Node* temp = new Node;
	temp->next = NULL;
	temp->name = head->name;

	round = true;

	while (round)
	{
		int dice = rand() % numberOfSoldiers + 1;
		cout << endl << head->name << " has drawn " << dice << endl;

		for (int i = 0; i < dice; i++)
		{
			temp->next = head;
			head = head->next;

			if (head == NULL)
			{
				temp->name = head->name;
				temp = head->next;
			}
		}

		cout << head->name << " has been removed.\n";

		printSoldiers(head);

		cout << endl;

		if (numberOfSoldiers == 1)
		{
			round = false;
		}
	}
}

int main()
{
	srand(time(NULL));
	Node* head = NULL;
	int i;
	int numberOfSoldiers;
	Node *soldiers = new Node;
	bool round = true;

	cout << "Lord Commander, How many soldiers do you want? ";
	cin >> numberOfSoldiers;

	for (i = 0; i < numberOfSoldiers; i++)
	{
		cout << "Name a Soldier: ";
		cin >> soldiers->name;
		head = sequence(soldiers, head);
	}
	cout << endl;

	printSoldiers(head);

	while (round)
	{
		cloakPassing(head, numberOfSoldiers, soldiers, round);
	}

	_getch();
	return 0;
}