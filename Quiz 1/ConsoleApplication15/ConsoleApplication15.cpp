// ConsoleApplication15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include "conio.h"
#include "string"
#include "iostream"
#include "time.h"

using namespace std;

void printPackages(int packages[], int sizeOfArray)	// ex 6 organizes the packages from smallest to biggest
{
	int i;

	for (i = 0; i <= sizeOfArray; i++)
	{
		for (int j = i + 1; j <= 7; j++)
		{
			int temporary;	

			if (packages[i] > packages[j])	// checks if "i" is greater than "j" if it is
			{
				temporary = packages[i];	// takes the "i" element and shelfs it into the temporary variable
				packages[i] = packages[j];	// "i" takes the next one 
				packages[j] = temporary;	// temporary then gets stored in "j" to check in the statement above again
			}
		}

	}

	for (i = 1; i <= 7; i++)
	{
		cout << "Package #" << i << ": " << packages[i] << endl; // this is to just add numbering to the packages
	}
}

void buyItem(int &userGold, int inputItem)	// function for buying item if usergold fulfills the input
{
	userGold = userGold - inputItem;
	cout << "Item Purchased!";

}

int recommendPackage(int &userGold, int arraySize, int packages[], int inputItem)	// recommends a package and also checks if it exceeds or not
{
	int i;
	char buyOrNah;
	int sumOfWant = 0;

	for (i = 0; i <= arraySize; ++i)
	{
		sumOfWant = packages[i] + userGold;
		if (sumOfWant >= inputItem)
		{
			cout << "\nYou don't have enough Gold for this item! Would you like to get Package #" << i << "? (Y/N)\n";
			cin >> buyOrNah;
			
			switch (buyOrNah)
			{
			case 'Y':
				cout << "Package and Item Purchased!\n";
				userGold = userGold + packages[i] - inputItem;	// adds current usergold and subtracts it to the input item 
				_getch();
				system("cls");
				return userGold;
			case 'N':
				cout << "You didn't purchase. :(\n";	// brings you back to the menu returning your gold
				_getch();
				system("cls");
				return userGold;
			}
		}
		
	}
		if (inputItem > sumOfWant)	// checks if a package is available 
		{
		cout << "Sorry! No Available Package for this. :(\n";
		_getch();
		system("cls");
		}
}

int main()
{
	int userGold = 250;
	int inputItem;
	int packages[7] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	char choice;

	while (true)
	{
		printPackages(packages, 7);

		cout << endl << "Current Gold: " << userGold << endl;
		cout << "Input price of the item you want to buy... ";
		cin >> inputItem;

		if (userGold >= inputItem)
		{
			buyItem(userGold, inputItem);
			cout << "\nDo you want to keep buying? (Y/N) ";
			cin >> choice;
			cout << endl;
			switch (choice)
			{
			case 'Y':
				true;	// repeats cycle
				break;
			case 'N':	// ends cycle
				cout << "Thank you for shopping!\n";
				_getch();
				system("cls");
				userGold = 250;	// restarts usergold into 250 again thus repeating the cycle
				break;
			}

		}
		else // goes into the recommend function if usergold < input
		{
			recommendPackage(userGold, 7, packages, inputItem);	

		}
	}

	_getch();
	return 0;
}
