#pragma once
#include "Skill.h"

class Heal : public Skill
{
public:
	Heal();
	~Heal();

	int addSkill() override;
	
private:
	int mHeal = 10;
};

