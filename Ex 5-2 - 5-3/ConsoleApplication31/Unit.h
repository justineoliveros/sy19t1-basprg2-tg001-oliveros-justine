#pragma once
class Unit
{
public:
	Unit(int hp, int pow, int vit, int dex, int agi);
	void viewStats();
	int getHp();
	int getPow();
	int getVit();
	int getDex();
	int getAgi();
	void setStats(int heal, int pow, int vit, int dex, int agi);
	~Unit();

private:
	int mHp;
	int mPow;
	int mVit;
	int mDex;
	int mAgi;
};

