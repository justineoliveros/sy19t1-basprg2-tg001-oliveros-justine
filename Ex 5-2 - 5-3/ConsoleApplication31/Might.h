#pragma once
#include "Skill.h"

class Might : public Skill
{
public:
	Might();
	~Might();

	int addSkill() override;

private:
	int mIncreasePow = 2;
};

