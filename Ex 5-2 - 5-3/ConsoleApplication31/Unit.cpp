#include "pch.h"
#include "Unit.h"
#include "iostream"

using namespace std;

Unit::Unit(int hp, int pow, int vit, int dex, int agi)
{
	mHp = hp;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;
}

void Unit::viewStats()
{
	cout << "HP: " << mHp << endl;
	cout << "POW: " << mPow << endl;;
	cout << "VIT: " << mVit << endl;
	cout << "DEX: " << mDex << endl;
	cout << "AGI: " << mAgi << endl;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getAgi()
{
	return mAgi;
}

void Unit::setStats(int heal, int pow, int vit, int dex, int agi)
{
	mHp = heal;
	mPow = pow;
	mVit = vit;
	mDex = dex;
	mAgi = agi;
}
Unit::~Unit()
{
}
