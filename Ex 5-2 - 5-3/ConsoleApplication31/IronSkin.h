#pragma once
#include "Skill.h"
class IronSkin : public Skill
{
public:
	IronSkin();
	~IronSkin();

	int addSkill() override;

private:
	int mIronSkin = 2;
};

