// ConsoleApplication31.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "Skill.h"
#include "vector"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Haste.h"
#include "Concentration.h"
#include "Unit.h"
#include "time.h"

using namespace std;

int main()
{
	srand(time(NULL));
	int hp = 20;
	int pow = 5;
	int vit = 5;
	int dex = 5;
	int agi = 5;
	Unit* player = new Unit(hp, pow, vit, dex, agi);
	vector<Skill*> skill;
	int skillChoice;
	Heal* heal = new Heal;
	Might* might = new Might;
	IronSkin* ironskin = new IronSkin;
	Concentration* concentration = new Concentration;
	Haste* haste = new Haste;
	skill.push_back(heal);
	skill.push_back(might);
	skill.push_back(ironskin);
	skill.push_back(concentration);
	skill.push_back(haste);
	int choice;

	while (true)
	{
		cout << "Skills:\n";
		cout << "[1] Heal\n";
		cout << "[2] Might\n";
		cout << "[3] Iron Skin\n";
		cout << "[4] Concentration\n";
		cout << "[5] Haste\n";
		cout << "[6] Random Choice\n";
		cout << "[0] View Stats\n";
		cout << "Your Choice: ";
		cin >> choice;

		if (choice == 6)
			choice = rand() % 5 + 1;

		else if (choice == 1)
		{
			int newHp = heal->addSkill() + player->getHp();
			hp = newHp;
			player->setStats(hp, pow, vit, dex, agi);
			player->viewStats();
		}

		else if (choice == 2)
		{
			int newPow = might->addSkill() + player->getPow();
			pow = newPow;
			player->setStats(hp, pow, vit, dex, agi);
			player->viewStats();
		}

		else if (choice == 3)
		{
			int newVit = ironskin->addSkill() + player->getVit();
			vit = newVit;
			player->setStats(hp, pow, vit, dex, agi);
			player->viewStats();
		}

		else if (choice == 4)
		{
			int newDex = might->addSkill() + player->getDex();
			dex = newDex;
			player->setStats(hp, pow, vit, dex, agi);
			player->viewStats();
		}

		else if (choice == 5)
		{
			int newAgi = might->addSkill() + player->getAgi();
			agi = newAgi;
			player->setStats(hp, pow, vit, dex, agi);
			player->viewStats();
		}

		else if (choice == 0)
		{
			player->viewStats();
		}

		system("pause");
		system("cls");
	}
}