#pragma once
#include "Skill.h"

class Concentration : public Skill
{
public:
	Concentration();
	~Concentration();

	int addSkill() override;

private:
	int mConcentrate = 2;
};

