#pragma once
#include "Skill.h"

class Haste : public Skill
{
public:
	Haste();
	~Haste();

	int addSkill() override;

private:
	int mHaste = 2;
};

