// ConsoleApplication19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "vector"
#include "time.h"
#include "conio.h"
#include "string"

using namespace std;

void pressAndClear() // utility function 
{
	system("pause");
	system("cls");
}

void deleteFunction(vector<string>& emperorSide, vector<string>& slaveSide, int round, int chosenCard) // delete function
{
	if (round <= 3 || round == 7 || round == 8 || round == 9)
	{
		if (chosenCard > 1)
		{
			emperorSide.erase(emperorSide.begin() + chosenCard);
		}
	}

	else if (round >= 4 || round >= 10)
	{
		if (chosenCard > 1)
		{
			slaveSide.erase(slaveSide.begin() + chosenCard);
		}
	}
}

void playerSideCards(vector<string>& emperorSide, vector<string>& slaveSide, int round, int chosenCard) // printing out the cards
{
	if (round <= 3 || round == 7 || round == 8 || round == 9) // prints out this if emperor
	{
		emperorSide.push_back("Emperor");

		for (unsigned int i = 0; i < 4; i++)
		{
			emperorSide.push_back("Civilian");
		}

		deleteFunction(emperorSide, slaveSide, round, chosenCard);

		for (unsigned int i = 0; i < emperorSide.size(); i++)
		{
			cout << "[" << i + 1 << "] " << emperorSide[i] << endl;
		}
		
		emperorSide.clear();
	}
	
	else if (round >= 4 || round >= 10) // prints out this if slave
	{
		slaveSide.push_back("Slave");
		
		for (unsigned int i = 0; i < 4; i++)
		{
			slaveSide.push_back("Civilian");
		}

		deleteFunction(emperorSide, slaveSide, round, chosenCard);

		for (unsigned int i = 0; i < slaveSide.size(); i++)
		{
			cout << "[" << i + 1 << "] " << slaveSide[i] << endl;

		}

		slaveSide.clear();
	}
}

void gameCondtion(vector<string>& emperorSide, vector<string>& slaveSide, int round, int& chosenCard, int& distanceWager, int& cash, int enemyRandomizer, int& distance) // game conditions. who wins, who loses.
{
	cout << "Pick a card to play\n";
	cout << "======================\n";
	
	playerSideCards(emperorSide, slaveSide, round, chosenCard);

	cin >> chosenCard;

	enemyRandomizer = rand() % 5 + 1;
	if (round <= 3 || round == 7 || round == 8 || round == 9) // emperor round
	{
		if (chosenCard == 1)
		{
			if (chosenCard == enemyRandomizer)
			{
				cout << "Kaiji picked Emperor, Tonegawa picked Slave. Tonegawa wins! Drill... drill...\n";
			}
			else if (chosenCard != enemyRandomizer)
			{
				cout << "Kaiji picked Emperor, Tonegawa picked Civilian. Kaiji wins!\n";

				cash = cash + (100000 * distanceWager);
				distance = distanceWager + distance;
			}
		}																					// conditions for emperor

		else if (chosenCard > 1)
		{
			if (enemyRandomizer == 1)
			{
				cout << "Kaiji picked Civilian, Tonegawa picked Slave. Kaiji wins!\n";
			
				cash = cash + (100000 * distanceWager); // payout
				distance = distanceWager + distance; // adds back wager if you win
			}
			else if (enemyRandomizer != 1)
			{
				cout << "Both picked Civilian. Draw!\n";
				pressAndClear();
				gameCondtion(emperorSide, slaveSide, round, chosenCard, distanceWager, cash, enemyRandomizer, distance);
			}
		}
	}
	else if (round >= 4 || round >= 10) // slave round
	{
		if (chosenCard == 1)
		{
			if (chosenCard == enemyRandomizer)
			{
				cout << "Kaiji picked Slave, Tonegawa picked Emperor. Kaiji wins!\n";

				cash = cash + (500000 * distanceWager);
				distance = distanceWager + distance;
			}
			else if (enemyRandomizer != 1)
			{
				cout << "Kaiji picked Slave, Tonegawa picked Civilian. Tonegawa wins! Drill... drill...\n";
			}
		}																									// conditions for slave

		else if (chosenCard > 1)
		{
			if (enemyRandomizer == 1)
			{
				cout << "Kaiji picked Civilian, Tonegawa picked Emperor. Tonegawa wins! Drill... drill...\n";
			}
			else if (enemyRandomizer != 1)
			{
				cout << "Both picked Civilian. Draw!\n";
				pressAndClear();
				gameCondtion(emperorSide, slaveSide, round, chosenCard, distanceWager, cash, enemyRandomizer, distance);
			}
		}
	}
}

void bettingScreen(int& cash, int& distance, int& round, int& distanceWager) // self explanatory... betting screen.
{
	cout << "Cash: " << cash << endl;
	cout << "Distance left (mm): " << distance << endl;
	cout << "Round " << round << "/12 " << endl;

	cout << "How many mm would you like to wager, Kaiji? ";
	cin >> distanceWager;

	if (distanceWager <= distance) // subtracts the amount of distance you wager if you lose
	{
		distance = distance - distanceWager;
	}
	else if (distanceWager > distance) // just an anti-stoopid if the player puts a number above the distance he has
	{
		cout << "Do you think this is a joke Kaiji? You know what's at stake... Play seriously...\n";
		pressAndClear();
		bettingScreen(cash, distance, round, distanceWager);
	}                                                                    
}

int main()
{
	srand(time(NULL));
	int cash = 0;
	int distance = 30;
	int distanceWager = 0;
	int round = 1;
	int chosenCard;
	int enemyRandomizer = rand() % 5 + 1;
	vector<string> emperorSide;
	vector<string> slaveSide;

	while (round <= 12)
	{
		bettingScreen (cash, distance, round, distanceWager);
		if (distanceWager != NULL)
		{
			pressAndClear();
			gameCondtion(emperorSide, slaveSide, round, chosenCard, distanceWager, cash, enemyRandomizer, distance);
			pressAndClear();
		}
		 if (round != 12)
		{
			 if (round <= 12 && cash >= 20000000 && distance > 0)
			 {
				 cout << "I'm a man of my word Kaiji... Congratulations on winning, or should I say... surviving?\n";
				 cout << "Cash: " << cash << endl;
				 cout << "Round Ended: " << round << endl;
				 cout << "Distance Left: " << distance << endl;
				 break;
			 }
			 else if (round == 12 && cash < 20000000 && distance > 0)
			 {																														// the endings
				 cout << "You have saved your ear, Kaiji... but did you save your friends?\n";
				 cout << "Cash: " << cash << endl;
				 cout << "Round Ended: " << round << endl;
				 cout << "Distance Left: " << distance << endl;
				 break;
			 }
			 else if (round <= 12 && cash < 20000000 && distance <= 0)
			 {
				 cout << "Lady Luck just wasn't at your side, Kaiji... just like your left ear... and friends.\n";
				 cout << "Cash: " << cash << endl;
				 cout << "Round Ended: " << round << endl;
				 cout << "Distance Left: " << distance << endl;
				 break;
			 }
			round++;
		}
	}
}