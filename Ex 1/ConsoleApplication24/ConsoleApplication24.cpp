#include "pch.h"
#include "string"
#include "iostream"
#include "conio.h"
#include "time.h"

using namespace std;
// 1
void sumOfFactorial(int &input, int &f)
{
	if (input <= 0)  // if invalid input
	{
		cout << "Invalid Number.\n"; 
	}
	else // factorial formula
	{
		for (int y = 1; y <= input; y++)
		{
			f = f * y;
		}
	}
}
// 2
void printItems()
{
	string items[8] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	for (int i = 0; i < 8; i++) // basic for loop to print out the elements in the array
	{
		cout << endl << items[i];
	}
}
// 3 
void printItemsInstances(string input)
{
	bool exist = false;
	int itemCounter = 0;
	string items[8] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };

	for (int i = 0; i < 8; i++)
	{
		if (input == items[i]) // statement that counts the number of times it sees the element in the array
		{
			exist = true;
			itemCounter++;
		}
	}

	if (exist) // output
	{
		cout << "You have " << itemCounter << " " << input;
	}
	else // if it doesn't exist
	{
		cout << input << " does not exist.";
	}

}
// 4
void fillAndShowArray()
{
	srand(time(NULL));
	int numbers[10] = {};
	int i;

	for (i = 0; i < 10; i++) // randomizer to have random numbers in the array
	{
		numbers[i] = rand() % 100 + 1;
	}

	for (i = 0; i < 10; i++) // display the numbers
	{
		cout << numbers[i] << endl;
	}
}
// 5
void fillAndShowArrayWithLargestNumber()
{
	srand(time(NULL));
	int numbers[10] = {};
	int i;

	for (i = 0; i < 10; i++) // randomizer
	{
		numbers[i] = rand() % 100 + 1; 
	}

	for (i = 0; i < 10; i++) // display
	{
		cout << numbers[i] << endl;
	}

	for (int i = 0; i < 10; ++i) // compares the element to the next element, if it's larger then it takes the element(number) and...
	{
		if (numbers[0] < numbers[i])
		{
			numbers[0] = numbers[i];
		}
	}

	cout << "Largest integer is " << numbers[0]; // displays it here

}
//6
void fillAndShowArrayOrder()
{
	srand(time(NULL));
	int numbers[10] = {};
	int i;
	int j;

	for (i = 0; i < 10; i++) // randomizerrr
	{
		numbers[i] = rand() % 100 + 1;
	}

	for (i = 0; i < 9; i++) // sorting system
	{
		for (j = i + 1; j < 10; j++)	// loop that goes ahead of the loop above to check the next element
		{
			int temporary; // placeholder declared

			if (numbers[i] > numbers[j])	// checks if the first loop element(number) is greater than the second loop element...
			{
				temporary = numbers[i];		// if it is greater than temporary becomes a place holder for the larger number...
				numbers[i] = numbers[j];	// first loop element will be the second loop element placing it ahead of the larger element(number)
				numbers[j] = temporary;		// second loop elemnt will inherit the placeholder's(int temporary) assigned element(number) 
			}
		}

	}

	for (i = 0; i < 10; i++) // display sorted elements(numbers)
	{
		cout << numbers[i] << endl;
	}

}

int main()
{
	/*1

	int inputFactorial;
	int f = 1;

	cout << "Input Number: ";
	cin >> inputFactorial;
	sumOfFactorial(inputFactorial, f);
	cout << inputFactorial << "!" << " = " << f;

	*/
	/*2

	string input;

	cout << "What do you want? ";
	getline(cin, input);
	printItems(input);

	*/
	/*3

	printItemsInstances();

	*/
	/*4

	fillAndShowArray();

	*/
	/*5

	fillAndShowArrayWithLargestNumber();

	*/

	/*6

	fillAndShowArrayOrder();

	*/


	_getch();
	return 0;
}

