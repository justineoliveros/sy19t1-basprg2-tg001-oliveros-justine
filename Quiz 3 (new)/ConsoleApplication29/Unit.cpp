#include "pch.h"
#include "Unit.h"
#include "Job.h"
#include "iostream"

using namespace std;

Unit::Unit(int classChoice)
{
	string className;

	if (classChoice == 1)
		className = "Warrior";
	if (classChoice == 2)
		className = "Assassin";
	if (classChoice == 3)
		className = "Mage";
	mName = "Enemy " + className;
	mHp = 20;
	mPow = rand() % 10 + 1;
	mVit = rand() % 10 + 1;
	mAgi = rand() % 10 + 1;
	mDex = rand() % 10 + 1;

}

Unit::Unit(string name)
{
	mName = name;
}

void Unit::viewStats()
{
	cout << "NAME: " << mName << endl;
	cout << "HP: " << mHp << endl;
	cout << "POW: " << mPow << endl;
	cout << "VIT: " << mVit << endl;
	cout << "AGI: " << mAgi << endl;
	cout << "DEX: " << mDex << endl;
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getDamage()
{
	return mDamage;
}

void Unit::unitAttack(Unit* actor, Unit* target)
{     
	cout << actor->getName() << " attacks " << target->getName() << endl;
	target->takeDamage(actor, target);
	cout << target->getName() << " received " << mDamage << " damage" << endl;
	cout << actor->getName() << " HP: " <<actor->getHp() << endl;
	cout << target->getName() << " HP: " << target->getHp() << endl;
} 

void Unit::takeDamage(Unit* actor, Unit* target)
{
	if (mDamage < 0) 
		return;

	mHp -= mDamage;
		
	if (mHp < 0)
		mHp = 0;
}

Unit::~Unit()
{
	cout << this << " is about to be deleted" << endl;
}
