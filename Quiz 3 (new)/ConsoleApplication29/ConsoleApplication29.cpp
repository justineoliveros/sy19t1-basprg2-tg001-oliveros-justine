// ConsoleApplication29.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "pch.h"
#include "time.h"
#include "string"
#include "Unit.h"
#include "conio.h"

using namespace std;

int main()
{
	srand(time(NULL));
	string name;
	int classChoice;

	cout << "Input Name: ";
	cin >> name;

	cout << endl;

	cout << "Pick a Class: \n";
	cout << "[1] Warrior\n";
	cout << "[2] Assassin\n";
	cout << "[3] Mage\n";
	cin >> classChoice;

	_getch();
	system("cls");
	Unit* player = new Unit(name);
	Unit* enemy = new Unit(rand() % 3 + 1);

	player->viewStats();

	cout << "\nvs\n\n";

	enemy->viewStats();
	
	_getch();
	system("cls");

	while (player->getHp() > 0 && enemy->getHp() > 0)
	{
		player->unitAttack(player, enemy);
		enemy->unitAttack(enemy, player);

		system("pause");
	}
}