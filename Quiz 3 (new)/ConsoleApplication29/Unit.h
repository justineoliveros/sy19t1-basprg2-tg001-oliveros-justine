#pragma once
#include "string"
#include "Job.h"
#include <iostream>

using namespace std;

class Unit 
{
public:
	Unit(int classChoice);
	Unit(string name);
	void viewStats();
	string getName();
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	int getDamage();
	void unitAttack(Unit* actor, Unit* target);
	void takeDamage(Unit* actor, Unit* target);
	~Unit();

private:
	string mName;
	int mHp = 20;
	int mPow = rand () % 10 + 1;
	int mVit = rand() % 10 + 1;
	int mAgi = rand() % 10 + 1;
	int mDex = rand() % 10 + 1;
	int mDamage = (mPow - mVit) + 5;
};

